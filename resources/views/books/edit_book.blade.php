<form method="post" action="/books/{{$book->id}}/edit" enctype="multipart/form-data">
	{{csrf_field()}}

	<div class="form-group">
		<label for="isbn">ISBN:</label><span class="redMark">*</span>
		<input type="number" name="isbn" class="form-control" required value="{{$book->isbn}}">
	</div>

	<div class="form-group">
		<label for="name">Name:</label><span class="redMark">*</span>
		<input type="text" name="name" class="form-control" required value="{{$book->name}}">	
	</div>

	<div class="form-group">
		<label for="author">Author:</label><span class="redMark">*</span>
		<input type="text" name="author" class="form-control" required value="{{$book->author}}">	
	</div>

	<div class="form-group">
		<label for="description">Description: </label>
		<textarea class="form-control" rows="7" name="description">{{$book->description}}
		</textarea>
	</div>

	<div class="form-group">
		<label for="publisher">Publisher:</label><span class="redMark">*</span>
		<input type="text" name="publisher" class="form-control" required value="{{$book->publisher}}">	
	</div>

	<div class="form-group">
		<label for="published_at">Publish Date:</label><span class="redMark">*</span>
		<input type="date" name="published_at" class="form-control" required value="{{$book->published_at}}">	
	</div>

	<div class="form-group">
		<label for="category_id">Categories:</label><span class="redMark">*</span>
		<select name="category_id" class="form-control">
			@foreach($categories as $category)
				@if($category->id == $book->category_id)
				<option value="{{$category->id}}" selected>{{$category->name}}</option>
				@else
				<option value="{{$category->id}}">{{$category->name}}</option>
				@endif
			@endforeach
		</select>
	</div>

	<div class="form-group">
		<label for="stock">Total Stock:</label>
		<input type="number" name="stock" min="0" class="form-control" value="{{$book->total_quantity}}">
	</div>

	<div class="form-group">
		<label for="image">Image:</label>
		<input type="file" name="image" class="form-control-file">
	</div>
	<button class="btn btn-primary float-right">Edit Book</button>

</form>
