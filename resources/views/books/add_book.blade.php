@extends('layouts.app')

@section('content')
<h1>Add Book</h1>

<form method="post" action="/books" enctype="multipart/form-data">
	{{csrf_field()}}

	<div class="form-group">
		<label for="isbn">ISBN:</label><span class="redMark">*</span>
		<input type="number" name="isbn" class="form-control" required value="{{ old('isbn') }}">
		@if(Session::has('error'))
			<span class="redMark">{{Session::get('error')}}</span>
		@endif
	</div>

	<div class="form-group">
		<label for="name">Name:</label><span class="redMark">*</span>
		<input type="text" name="name" class="form-control" required value="{{ old('name') }}">	
	</div>

	<div class="form-group">
		<label for="author">Author:</label><span class="redMark">*</span>
		<input type="text" name="author" class="form-control" required value="{{ old('author') }}">	
	</div>

	<div class="form-group">
		<label for="description">Description: </label>
		<textarea class="form-control" rows="5" name="description" value="{{ old('description') }}">
		</textarea>
	</div>

	<div class="form-group">
		<label for="publisher">Publisher:</label><span class="redMark">*</span>
		<input type="text" name="publisher" class="form-control" required value="{{ old('publisher') }}">	
	</div>

	<div class="form-group">
		<label for="published_at">Publish Date:</label><span class="redMark">*</span>
		<input type="date" name="published_at" class="form-control" required value="{{ old('published_at') }}">	
	</div>

	<div class="form-group">
		<label for="category_id">Categories:</label><span class="redMark">*</span>
		<select name="category_id" class="form-control">
			@foreach($categories as $category)
				<option 
					value="{{$category->id}}" 
					{{ old('category_id') == $category->id ? 'selected' : '' }}>
					{{$category->name}}
				</option>

			@endforeach
		</select>
	</div>

	<div class="form-group">
		<label for="stock">Total Stock:</label><span class="redMark">*</span>
		<input type="number" name="stock" min="0" class="form-control" required 
		value="{{ old('stock') }}"
		>
	</div>

	<div class="form-group">
		<label for="image">Image:</label>
		<input type="file" name="image" class="form-control-file">
	</div>

	<button class="btn btn-primary float-right" id="addBookBtn">Add Items</button>

</form>
@endsection