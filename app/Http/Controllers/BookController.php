<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Category;
use App\User;
use Auth; 
use Session;

class bookController extends Controller
{
    function index(Request $request){
        $search = $request->search;
    	if($request->get('search') != null){
            $name = Book::where('name','like', "%$request->search%")->get();
            $author = Book::where('author','like', "%$request->search%")->get();
            $books = $name->merge($author);
    	}
    	else{
    		$books = Book::all();
    	}
    	return view('books.book_list', compact('books', 'search'));
    }

    function create(){
    	$categories = Category::all();
    	return view('books.add_book', compact('categories'));
    }

    function store(Request $request){
        // Check ISBN
        if (strlen($request->isbn) != 10) {
            Session::flash('error', "Invalid ISBN"); //Send Message
            return redirect()->back()->withInput();
        }
        $new_book = new Book();
        $new_book->isbn = $request->isbn;
        $new_book->name = $request->name;
        $new_book->author = $request->author;
        $new_book->publisher = $request->publisher;
        $new_book->published_at = $request->published_at;
        $new_book->description = $request->description;
        $new_book->category_id = $request->category_id;
        $new_book->total_quantity = $request->stock;
        $new_book->stock = $request->stock;
        if($request->hasFile('image')){
            $image = $request->image;
            $image->move('assets/images/', $image->getClientOriginalName());
            $new_book->image = 'assets/images/'.$image->getClientOriginalName();
        }
        else{
            $new_book->image = 'assets/images/No_picture_available.png';
        }
        $new_book->save();
        Session::flash('message', "Item Added Successfully"); //Send Message
        return redirect('/books');
    }

    function show($id){
        $book = Book::find($id);
        return view('books.detail', compact('book'));
    }

    function getEditBook($id){
        $book = Book::find($id);
        $categories = Category::all();
        return view('books.edit_book', compact('categories', 'book'));
    }

    function update($id, Request $request){
        $book = Book::find($id);
        $old_quantity = $book->total_quantity;

        if($request->hasFile('image')){
            $image = $request->image;
            $image->move('assets/images/', $image->getClientOriginalName());
            $book->image = 'assets/images/'.$image->getClientOriginalName();
        };
        $book->isbn = $request->isbn;
        $book->name = $request->name;
        $book->author = $request->author;
        $book->publisher = $request->publisher;
        $book->published_at = $request->published_at;
        $book->description = $request->description;
        $book->category_id = $request->category_id;
        $book->total_quantity = $request->stock;
        $book->stock += ($request->stock-$old_quantity);
        $book->save();
        return redirect('/books');
    }

    function destroy($id){
        Book::find($id)->delete();
        Session::flash('message', "Book Deleted Successfully"); //Send Message
    }

    function restore($id){
        $book = Book::onlyTrashed()->find($id)->restore();
        return redirect('/books');
    }

    function returnList(){
        $user = User::find(Auth::user()->id);
        $books_returning = $user->books()->wherePivot('status','=', 1)->get();
        $books_pending = $user->books()->wherePivot('status','=', 0)->get();
        return view('books.book_return', compact('books_pending', 'books_returning'));
    }
}
