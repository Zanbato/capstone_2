<form method="post" action="/books/borrow/{{$id}}">
	{{csrf_field()}}

	<div class="form-group">
		<label for="user">Name:</label>
		<input type="text" name="user" class="form-control" disabled value="{{$user}}">	
	</div>

	<div class="form-group">
		<label for="book">Name of the book:
		<input type="text" name="book" class="form-control" disabled 
		value="{{$book}}" data-id="{{$id}}">	
	</div>

	<div class="form-group">
		<label for="date">Date:</label>
		<input type="text" name="date" class="form-control" readonly value="{{$date}}">	
	</div>
	<div class="form-group">
	    <label for="quantity">Quantity</label>
	    <select class="form-control" id="quantity" name="quantity">
	      @for($i = 1; $i <= $book_stock; $i++)
	      	<option>{{$i}}</option>
	      @endfor
	    </select>
 	</div>

	<button class="btn btn-primary float-right">Submit</button>
	
</form>