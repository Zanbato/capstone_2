<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Book;
use Session;

class CategoryController extends Controller
{
    function index(){
    	$categories = Category::all();
        $deleted = Category::onlyTrashed()->get();
    	return view('categories.category_list', compact('categories', 'deleted'));
    }

    function store(Request $request){
    	if(Category::where('name', "$request->category")->exists()){
            Session::flash('error', "Category Already Existed"); //Send Message
            return redirect('/categories');
        }
        $new_category = new Category();
        $new_category->name = $request->category;
        $new_category->save();
        Session::flash('message', "New Category Added"); //Send Message
        return redirect('/categories');
    }

    function update($id, Request $request){
        $category = Category::find($id);
        $category->name = $request->name;
        $category->save();
        Session::flash('message', "Item Added Successfully"); //Send Message
        return redirect('/categories');
    }

    function destroy($id){
        $books=Category::find($id)->books;
        foreach ($books as $book) {
            $book->category_id = 11;
            $book->save();
        }
        Category::find($id)->delete();
    	Session::flash('error', "Category Removed"); //Send Message
    }

    function restore($id){
        Category::onlyTrashed()->find($id)->restore();
        return redirect('/categories');
    }
}
