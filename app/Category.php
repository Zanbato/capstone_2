<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Category extends Model
{
	use SoftDeletes;

   function books(){
    	return $this->hasMany('App\Book');
    }

    protected $dates = [
        'deleted_at'
    ];
}
