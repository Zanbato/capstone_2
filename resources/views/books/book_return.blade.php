@extends('layouts.app')

@section('content')
<div class="card m-4">
		<div class="card-header bg-dark text-white">
			My Books
		</div>
		<div class="card-body">
			<h2>Pending</h2>
			@if($books_pending->isEmpty())
				<p>You have no pending books.</p>
			@else
				<table class="table table-striped">
					<thead>
						<tr>
							<th colspan="">Books</th>
							<th colspan="">Author</th>
							<th colspan="">Quantity</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($books_pending as $book)
							<tr id="row{{$book->id}}">
								<td class="bookName">
									{{$book->name}}
								</td>
								<td class="bookAuthor">
									{{$book->author}}
								</td>
								<td class="bookQuantity">
									{{$book->pivot->quantity}}
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			@endif
			<h2 class="pt-5">Return</h2>
			@if($books_returning->isEmpty())
				<p>You have no returning books.</p>
			@else
				<table class="table table-striped">
					<thead>
						<tr>
							<th colspan="">Books</th>
							<th colspan="">Author</th>
							<th colspan="">Quantity</th>
							<th colspan="">Date Borrowed</th>
							<th colspan="3">Date Due</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($books_returning as $book)
							<tr id="row{{$book->id}}">
								<td class="bookName">
									{{$book->name}}
								</td>
								<td class="bookAuthor">
									{{$book->author}}
								</td>
								<td class="bookQuantity">
									{{$book->pivot->quantity}}
								</td>
								<td class="dateBorrowed">
									{{date('m/d/Y', strtotime($book->pivot->updated_at))}}
								</td>
								<td class="dateDue">
									{{date('m/d/Y', strtotime($book->pivot->updated_at.' + 7 days'))}}
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			@endif
		</div>
	</div>
@endsection