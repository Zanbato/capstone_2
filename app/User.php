<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    function books(){
        return $this->belongsToMany('App\Book')
        ->withPivot(['quantity', 'status', 'id', 'updated_at']);
    }

    function pendingBooks(){
        return $this->books()->wherePivot('status', 0);
    }

    function returningBooks(){
        return $this->books()->wherePivot('status', 1);
    }

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'deleted_at'
    ];
}
