@extends('layouts.app')

@section('content')
	<form method="get" action="/books">
	    	<label for="formGroupExampleInput">Search Books</label>
	  <div class="form-row pb-4">
	  	<div class="col-11">
	    	<input type="text" class="form-control" id="formSearchBooks" name="search" placeholder="Search Books..." value="{{$search}}">
	  	</div>
	  	<div class="col">
	    	<button class="btn btn-primary" id="searchBooks" disabled>Submit</button>
	  	</div>
	  </div>
	</form>
	@if(Session::has('message'))
			<div class="alert alert-success alert-dismissible fade show" role="alert">
				{{Session::get('message')}}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
	@endif
	@if($books->isEmpty())
		<h2>Sorry, we couldn't find any results matching "{{$search}}"</h2>
		<hr>
		<h4>Suggestions:</h4>
		<ul>
			<li>Make sure all words spelled correctly.</li>
			<li>Try different keywords.</li>
			<li>Try more general keywords.</li>
		</ul>
	@else
	<div id="bookList" class="row">
		@foreach($books as $book)
			<div class="col-lg-3 col-md-6 pt-3">
				<div class="card bookShow" data-id="{{$book->id}}" data-name="{{$book->name}}">
					<div class="bookDetail" data-toggle="modal" data-target="#modalDetails">
						<img src="{{$book->image}}">
						<div class="alignTextBook">
							<h5>{{$book->name}}</h5>
							<h6>by {{$book->author}}</h6>
						</div>
					</div>
					<div class="card-footer">
						<p>Available Stock: {{$book->stock}}</p>
					</div>
					@if(Auth::user() && Auth::user()->role_id == 1)
						<button class="btn btn-danger deleteBook w-100" data-toggle="modal" data-target="#modalDelete">Delete</button>
					@endif
				</div>
			</div>
		@endforeach
	</div>
	@endif

{{-- Modal Book details --}}
	<div class="modal fade" id="modalDetails" tabindex="-1" role="dialog">
	  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
	    <div class="modal-content">

	    	<!-- Modal Header -->
	      <div class="modal-header">
	        <h5 class="modal-title">Book</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>

	      <!-- Modal Body -->
	      <div class="modal-body" id="detailModalBody"></div>

	      <!-- Modal Footer -->
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        @if(Auth::user() && Auth::user()->role_id == 1)
	        	<button data-dismiss="modal" type="button" class="btn btn-primary" id="editBook" data-toggle="modal" data-target="#modalEdit">Edit</button>
	        @elseif(Auth::user() && Auth::user()->role_id == 2)
	        	<button data-dismiss="modal" type="button" class="btn btn-warning" id="borrowBook" data-toggle="modal" data-target="#modalBorrow">Borrow</button>
	        @endif
	      </div>

	    </div>
	  </div>
	</div>

{{-- Modal Book Edit --}}
	<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">

	    	<!-- Modal Header -->
	      <div class="modal-header ">
	        <h5 class="modal-title">Edit</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>

	      	<!-- Modal Body -->
	      <div class="modal-body" id="editModalBody"></div>

	    </div>
	  </div>
	</div>

{{-- Modal Book Borrow --}}
	<div class="modal fade" id="modalBorrow" tabindex="-1" role="dialog">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">

	    	<!-- Modal Header -->
	      <div class="modal-header ">
	        <h5 class="modal-title">Borrow</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>

	      	<!-- Modal Body -->
	      <div class="modal-body" id="borrowModalBody"></div>

	    </div>
	  </div>
	</div>

{{-- Modal Book delete --}}
	<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
	  <div class="modal-dialog .modal-dialog-centered " role="document">
	    <div class="modal-content">

	    	<!-- Modal Header -->
	      <div class="modal-header ">
	        <h5 class="modal-title">Remove Book?</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>

	      	<!-- Modal Body -->
	      <div class="modal-body" id="editModalBody">Are you sure you want to delete "<strong><span id="bookTitle"></span></strong>" from Libraria?</div>

	      <!-- Modal Footer -->
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-danger" id="confirmDelete" data-dismiss="modal">Confirm Delete</button>
	      </div>

	    </div>
	  </div>
	</div>


@endsection