@extends('layouts.app')

@section('content')
			<div class="card">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th colspan="2">Date Created</th>
						</tr>
					</thead>
					@foreach ($users as $user)
						@if($user->status == 0)
							<tr data-id="{{$user['id']}}" data-name="{{$user->name}}">
								<td class="itemcategory">
									<p>{{$user['name']}}</p>
									<input type="hidden" class="input-group form-control" type="text" name="user" value="{{$user['name']}}">
								</td>
								<td class="itemcategory">
									<p>{{$user['email']}}</p>
								</td>
								<td class="itemcategory">
									<p>{{$user->created_at}}</p>
								</td>
								<td class="buttons" align="right">
									<button 
										class="btn btn-success approveUser">
										Approve
									</button>
									<button 
										class="btn btn-danger deleteUser" 
										data-toggle="modal"
										data-target="#modalDelete">
										Delete
									</button>
								</td>
							</tr>
						@endif
					@endforeach
				</table>
			</div>

{{-- Modal Book delete --}}
	<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
	  <div class="modal-dialog .modal-dialog-centered " role="document">
	    <div class="modal-content">

	    	<!-- Modal Header -->
	      <div class="modal-header ">
	        <h5 class="modal-title">Remove User?</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>

	      	<!-- Modal Body -->
	      <div class="modal-body" id="editModalBody">Are you sure you want to delete "<strong><span id="userName"></span></strong>" from Libraria?</div>

	      <!-- Modal Footer -->
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-danger" id="confirmDelete" data-dismiss="modal">Confirm Delete</button>
	      </div>

	    </div>
	  </div>
	</div>

@endsection