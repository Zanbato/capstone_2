<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Book;
use App\Category;
use Session;
use Auth;

class UserController extends Controller
{
    function index(){
    	$users = Auth::user()->all();
    	return view('users.user_list', compact('users'));
    }

    function bookList(){
    	$users = User::has('books')->get();
 		return view('users.borrow_list', compact('users'));
    }

    function approve(Request $request){
    	$id = $request->id;
    	$user = $request->user;
    	$quantity = $request->quantity;
    	$book = User::find($user)->books()
    				->wherepivot('id', $id)
    				->first();
    	if ($book->stock >= $quantity) {
    		$book->pivot->status = 1;
    		$book->stock -= $quantity;
    		$book->save();
    		$book->pivot->save();
    		Session::flash('message', "Approved"); //Send Message
    	}else{
    		Session::flash('error', "Over Quantity/Out of Stock"); //Send Message
    	}
    }

    function deny(Request $request){
    	$user_id = $request->user;
    	$pivot_id = $request->id;
    	$book_id = User::find($user_id)->books()
    				->wherepivot('id', $pivot_id)
    				->first()->id;
    	$user = User::find($user_id);
    	$user->books()->wherepivot('status', 0)->detach($book_id);
    }

    function acceptUser(Request $request){
        $id = $request->id;
        $user = User::find($id);
        $user->status = 1;
        $user->save();
    }

    function destroyUser(Request $request){
        User::find($request->id)->delete();
        return redirect('/users');
    }

    function __construct(){
        $this->middleware('isAdmin');
    }
}
