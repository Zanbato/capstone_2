<div class="row">
	<div class="col">
		<img src="{{$book->image}}" width="100%">
	</div>
	<div class="col">
		<h2>
			{{$book->name}}
		</h2>
		By {{$book->author}}
		<hr>
		<p>{!!nl2br(e($book->description))!!}</p>
		<div class="small">Category: {{$book->category->name}}</div>
		<div class="small">ISBN: {{$book->isbn}}</div>
		<div class="small">Publisher: {{$book->publisher}}</div>
		<div class="small">Published: {{$book->published_at}}</div>
	</div>
</div>
