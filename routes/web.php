<?php
Route::get('/', function () {
    return view('welcome');
});
Route::middleware('isAdmin')->group(function(){
	Route::get('/books/create', 'BookController@create');
	Route::get('/books/create/categories', 'BookController@createCategories');
	Route::get('/users', 'UserController@index');
	Route::post('/users', 'UserController@acceptUser');
	Route::delete('/users', 'UserController@destroyUser');
	Route::get('/users/borrow_list', 'UserController@bookList');
	Route::post('/users/borrow_list', 'UserController@approve');
	Route::delete('/users/borrow_list', 'UserController@deny');
	Route::get('/categories/{id}/restore', 'CategoryController@restore'); 
	Route::post('/books', 'BookController@store');
	Route::get('/books/{id}/edit', 'BookController@getEditBook');
	Route::post('/books/{id}/edit', 'BookController@update');
	Route::get('/books/{id}/delete', 'BookController@destroy');
	Route::get('/books/{id}/restore', 'BookController@restore'); //Still in progress
	Route::post('/categories/{id}/edit', 'CategoryController@update'); 
	Route::get('/categories/{id}/delete', 'CategoryController@destroy');
});

Route::get('/books', 'BookController@index');
Route::get('/books/return', 'BookController@returnList');
Route::post('/books/return', 'BookRequestController@return');
Route::get('/books/borrow/{id}', 'BookRequestController@create');
Route::post('/books/borrow/{id}', 'BookRequestController@store');
Route::get('/books/{id}', 'BookController@show');

Route::get('/categories', 'CategoryController@index'); 
Route::post('/categories', 'CategoryController@store'); 



Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
