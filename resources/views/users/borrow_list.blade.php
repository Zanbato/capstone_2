@extends('layouts.app')

@section('content')
	@if(Session::has('message'))
			<div class="alert alert-success alert-dismissible fade show">
				{{Session::get('message')}}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
	@elseif(Session::has('error'))
			<div class="alert alert-danger alert-dismissible fade show">
				{{Session::get('error')}}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
	@endif
	<div class="card m-4">
		<div class="card-header bg-dark text-white">
			Borrow List
		</div>
		<div class="card-body">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>ID#</th>
							<th>Name</th>
							<th>Book</th>
							<th>Date</th>
							<th colspan="2">Quantity</th>
						</tr>
					</thead>
					<tbody id="">
						@foreach ($users as $user)
							@foreach($user->pendingBooks as $book)
							<tr id="row{{$book->pivot->id}}" 
								data-id="{{$book->pivot->id}}" 
								data-quantity="{{$book->pivot->quantity}}"
								data-user="{{$user->id}}">
								<td class="id">
									{{$book->pivot->id}}
								</td>
								<td>
									{{$user->name}}
								</td>
								<td class="bookName">
									{{$book->name}}
								</td>
								<td>
									{{$book->pivot->updated_at}}
								</td>
								<td>
									{{$book->pivot->quantity}}
								</td>
								<td class="" align="right">
									<button class="btn btn-success approveBorrow">
										Approve
									</button>
									<button 
										class="btn btn-secondary denyBorrow">
										Deny
									</button>
								</td>
							</tr>
							@endforeach
						@endforeach
					</tbody>
				</table>
		</div>
	</div>
	<div class="card m-4">
		<div class="card-header bg-dark text-white">
			Return list
		</div>
		<div class="card-body">
			<table class="table table-striped">
				<thead>
					<tr>
						<th colspan="">Name</th>
						<th colspan="">Book</th>
						<th colspan="">Author</th>
						<th colspan="">Date Borrowed</th>
						<th colspan="">Date Due</th>
						<th colspan="2">Quantity</th>
					</tr>
				</thead>
				<tbody id="pendingTask">
					@foreach ($users as $user)
						@foreach($user->returningBooks as $book)
						<tr id="row{{$book->pivot->id}}" 
							data-id="{{$book->pivot->id}}" 
							data-quantity="{{$book->pivot->quantity}}"
							data-user="{{$user->id}}">
							<td class="userName">
								{{$user->name}}
							</td>
							<td class="bookName">
								{{$book->name}}
							</td>
							<td class="bookAuthor">
								{{$book->author}}
							</td>
							<td class="dateBorrowed">
								{{date('m/d/Y', strtotime($book->pivot->updated_at))}}
							</td>
							<td class="dateDue">
								{{date('m/d/Y', strtotime($book->pivot->updated_at.' + 7 days'))}}
							</td>
							<td class="bookQuantity">
								<select class="form-control">
									@for($i = $book->pivot->quantity; $i > 0; $i--)
								    	<option>{{$i}}</option>
								    @endfor
								</select>
							</td>
							<td class="return" align="right">
								<button 
									class="btn btn-warning returnBooks" 
									type="submit" 
									value= "{{$book->pivot->id}}">
									Return Book
								</button>
								<input type="hidden" name="pivot_id" value="{{$book->pivot->id}}">
								<input type="hidden" name="quantity" value="1">
								<input type="hidden" name="user_id" value="{{$user->id}}">
							</td>
						</tr>
						@endforeach
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection