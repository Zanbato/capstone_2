@extends('layouts.app')

@section('content')
	@if(Session::has('message'))
			<div class="alert alert-success alert-dismissible fade show" role="alert">
				{{Session::get('message')}}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
	@elseif(Session::has('error'))
			<div class="alert alert-danger alert-dismissible fade show">
				{{Session::get('error')}}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
	@endif

	<div class="accordion" id="accordionCategory">

		<div class="card m-4">
			<div class="card-header bg-dark text-white">
				<div class="row">
				<div class="col">Categories</div>
				@if(Auth::user() && Auth::user()->role_id == 1)
					<div class="col text-right">
						<i class="fas fa-plus-circle fa-lg" 
						data-toggle="collapse" 
						data-target="#collapseOne" 
						aria-expanded="true" 
						aria-controls="collapseOne"></i>
					</div>
				@endif
				</div>
			</div>
			@if(Auth::user() && Auth::user()->role_id == 1)
			    <div id="collapseOne" class="collapse" data-parent="#accordionCategory">
			      <div class="card-body">
			        <form method="post" action="/categories" class="pl-3">
						{{csrf_field()}}
						<div class="form-row text-justify">
							<div class="col-11">
								<input class="form-control " type="text" name="category" placeholder="New Category" id="categoryInput">
							</div>
							<div class="col">
								<button class="btn btn-secondary" type="submit" id="addCatBtn" disabled>Add</button>
							</div>
						</div>
					</form>
			      </div>
			    </div>
		    @endif
			<div class="card-body">

				<table class="table table-striped">
					@foreach ($categories as $category)
						<tr>
							<td class="itemcategory">
								<p>{{$category['name']}}</p>
								<input type="hidden" class="input-group form-control" type="text" name="category" value="{{$category['name']}}">
							</td>
							<td class="buttons" data-id="{{$category['id']}}" data-name="{{$category['name']}}" align="right">
								@if(Auth::user() && Auth::user()->role_id == 1)
									@if($category->id != 11)
										<button 
											class="btn btn-primary categoryEdit">
											Edit
										</button>
										<button 
											class="btn btn-success saveCategoryEdit" 
											hidden>
											Save
										</button>
										<button 
											class="btn btn-danger deleteCategory" 
											value= "{{$category['id']}}"
											name="id"
											data-toggle="modal"
											data-target="#modalDelete">
											Delete
										</button>
									@endif
								@endif
							</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
	</div>

	@if(Auth::user() && Auth::user()->role_id == 1)
		<div class="card m-4">
				<div class="card-header bg-dark text-white">
					<div class="row">
						<div class="col">Removed Categories</div>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-striped">
						@foreach ($deleted as $category)
							<tr>
								<td class="itemcategory">
									<p>{{$category['name']}}</p>
									<input type="hidden" class="input-group form-control" type="text" name="category" value="{{$category['name']}}">
								</td>
								<td class="buttons" data-id="{{$category['id']}}" data-name="{{$category['name']}}" align="right">
									<button 
										class="btn btn-success restoreCategory">
										Restore
									</button>
								</td>
							</tr>
						@endforeach
					</table>
				</div>
			</div>
	@endif


{{-- Modal Book delete --}}
	<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
	  <div class="modal-dialog .modal-dialog-centered " role="document">
	    <div class="modal-content">

	    	<!-- Modal Header -->
	      <div class="modal-header ">
	        <h5 class="modal-title">Remove Category</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>

	      	<!-- Modal Body -->
	      <div class="modal-body" id="editModalBody">Are you sure you want to delete "<strong><span id="categoryTitle"></span></strong>" from Libraria?</div>

	      <!-- Modal Footer -->
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-danger" id="confirmDelete" data-dismiss="modal">Confirm Delete</button>
	      </div>

	    </div>
	  </div>
	</div>

@endsection