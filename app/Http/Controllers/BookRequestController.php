<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\User;
use Auth;
use Session;

class BookRequestController extends Controller
{
    function create($id){
        $date = date("M d, Y");
        $book = Book::find($id)->name;
        $book_stock = Book::find($id)->stock;
        $user = Auth::user()->name;
        return view('books.borrow_book', compact('date','book','user', 'id', 'book_stock'));
    }

    function store($book_id, Request $request){
        $user = User::find(Auth::user()->id);
        $book = $user->books();
        $quantity = $request->quantity;
        $book_checker = $book->wherePivot('book_id', $book_id);

        // Check if the book already existed
        if ($book_checker->exists() && $book_checker->first()->pivot->status == 0){
            $user->books()->updateExistingPivot($book_id, ['quantity' => ($quantity)]);
        }
        else{
        $user->books()->attach($book_id, ['quantity' => $quantity]);
        }

        return redirect('/books');
    }

    function return(Request $request){
        $user = User::find($request->user_id);
        $book = $user->books()->wherePivot('id', $request->pivot_id)->first();
        $quantity = $request->quantity;
        $book->stock += $quantity;
        $book->save();
        // Check quantity
        $pivot_quantity = $book->pivot->quantity - $quantity;
        if ($pivot_quantity != 0) {
            $user->books()->updateExistingPivot($book->id, ['quantity' => ($pivot_quantity)]);
        }
        else{
            $user->books()->wherePivot('id', $request->pivot_id)->detach();
        }
            Session::flash('message', "Books Returned Succesfully"); //Send Message
    }
}
